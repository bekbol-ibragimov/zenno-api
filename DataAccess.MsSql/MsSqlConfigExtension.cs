﻿using System;
using DataAccess.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace DataAccess.MsSql
{
    public static class MsSqlConfigExtension
    {
        public static void RegisterMsDb(this IServiceCollection services, IConfiguration configuration)
        {
            var connection = configuration.GetConnectionString("MsSqlConnection");
            Console.WriteLine("MsSqlConnection: {0}", connection);
            services.AddDbContext<IMsDbContext, MsDbContext>(options =>
                options.UseSqlServer( 
                    connection,
                    b => b.MigrationsAssembly(typeof(MsDbContext).Assembly.FullName)));

        }
    }
}