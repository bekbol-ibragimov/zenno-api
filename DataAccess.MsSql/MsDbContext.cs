﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using DataAccess.Interfaces;
using Entities.Models;
using Entities.Models.Auth;
using Entities.Models.Dictionaries;
using Entities.Models.Files;
using Entities.Models.Global;
using Microsoft.EntityFrameworkCore;
using WebApp.Interfaces.Services;

namespace DataAccess.MsSql
{
    public class MsDbContext : DbContext, IMsDbContext
    {
        private readonly IDateTimeService _dateTime;
        private readonly IHttpContextService _httpContext;
        public MsDbContext(DbContextOptions<MsDbContext> options, IDateTimeService dateTime, IHttpContextService httpContext)
            : base(options)
        {
            _dateTime = dateTime;
            _httpContext = httpContext;
        }
        #region Auth
        public DbSet<User> AuthUsers { get; set; }
        public DbSet<UserRole> AuthUserRoles { get; set; }
        public DbSet<UserPermission> AuthUserPermissions { get; set; }
        public DbSet<Role> AuthRoles { get; set; }
        public DbSet<RolePermission> AuthRolePermissions { get; set; }
        public DbSet<Permission> AuthPermissions { get; set; }
        #endregion Auth
        
        #region Dictionary
        public DbSet<Company> DicCompanies { get; set; }
        public DbSet<Country> DicCountries { get; set; }
        public DbSet<Localization> DicLocalizations { get; set; }
        
        public DbSet<TestEntity> TestEntities { get; set; } 
        #endregion

        
        #region Files
        public DbSet<FileTemp> FileTemps { get; set; }
        #endregion Files
        
        public DbSet<Question> Questions { get; set; }
        
        public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = new CancellationToken())
        {
            var dt = _dateTime.NowUtc;
            foreach (var entry in ChangeTracker.Entries<BaseEntity>())
            {
                switch (entry.State)
                {
                    case EntityState.Added:
                        entry.Entity.CreatedAt = dt;
                        entry.Entity.CreatedUserId = _httpContext.UserId;
                        entry.Entity.UpdatedAt = dt;
                        entry.Entity.UpdatedUserId = _httpContext.UserId;
                        break;
                    case EntityState.Modified:
                        entry.Entity.UpdatedAt = dt;
                        entry.Entity.UpdatedUserId = _httpContext.UserId;
                        break;
                }
            }
            return base.SaveChangesAsync(cancellationToken);
        }
        
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            foreach (var relationship in modelBuilder.Model.GetEntityTypes().SelectMany(e => e.GetForeignKeys()))
            {
                relationship.DeleteBehavior = DeleteBehavior.Restrict;
            }

            base.OnModelCreating(modelBuilder);

        }
    }
}