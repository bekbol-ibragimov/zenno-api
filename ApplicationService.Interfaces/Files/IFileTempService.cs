﻿using System;
using System.Threading.Tasks;
using ApplicationService.Interfaces.Enums;

namespace ApplicationService.Interfaces.Files
{
    public interface IFileTempService
    {
        Task<string> CopyToPath( Guid fileId, FilePath newPath);
    }
}