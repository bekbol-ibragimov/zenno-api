using System.Threading.Tasks;
using ApplicationService.Interfaces.Auth.Dto.Sign.Response;

namespace ApplicationService.Interfaces.Auth
{
    public interface ISignFileControlService
    {
     
        /// <summary>
        /// Проверка подписи на валидность
        /// </summary>
        /// <param name="singBase64"></param>
        /// <returns></returns>
        Task<SignResponseDto<SignResultDto>> ValidateSign(string singBase64);

        Task<SignExtractResponseDto> ExtractRaw(string singBase64);
        
        /// <summary>
        /// Получение текста ошибки из кода
        /// </summary>
        /// <param name="status"></param>
        /// <returns></returns>
        string GetSingErrorMessage(int status);
    }
}