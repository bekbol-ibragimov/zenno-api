﻿using Newtonsoft.Json;

namespace ApplicationService.Interfaces.Auth.Dto.Sign.Request
{
    public class SignCheckRequestDto : BaseSignRequestDto
    {
        [JsonProperty("params")] public SignCheckParamDto Params { get; set; }
    }
}