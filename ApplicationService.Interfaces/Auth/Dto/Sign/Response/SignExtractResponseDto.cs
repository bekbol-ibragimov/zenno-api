using Newtonsoft.Json;

namespace ApplicationService.Interfaces.Auth.Dto.Sign.Response
{
    public class SignExtractResponseDto
    {
        [JsonProperty("originalData")]
        public string Data { get; set; }
        
        /// <summary>
        /// Сообщение
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// Статус
        /// </summary>
        public int Status { get; set; }
    }
}