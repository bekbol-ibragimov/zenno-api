﻿using System;
using System.Collections.Generic;

namespace ApplicationService.Interfaces.Auth.Dto.Sign.Response
{
    public class SignResultDto
    {
        public IEnumerable<Signer> Signers { get; set; }
        
    }

    public class Signer
    {
        public CertDto Cert { get; set; }
    }
    
    public class CertDto
    {
        public List<ChainDto> Chain { get; set; }
        public string SerialNumber { get; set; }
        public SubjectDto Subject { get; set; }
        public string SignAlg { get; set; }
        public bool Valid { get; set; }
        public string KeyUsage { get; set; }
    }

    public class ChainDto
    {
        public bool Valid { get; set; }
        public DateTime NotAfter { get; set; }
        public string KeyUsage { get; set; }
        public string SerialNumber { get; set; }
        public SubjectDto Subject { get; set; }
    }

    public class SubjectDto
    {
        public string Country { get; set; }
        public string CommonName { get; set; }
        public string Gender { get; set; }
        public string Surname { get; set; }
        public string Locality { get; set; }
        public string Dn { get; set; }
        public string State { get; set; }
        public string BirthDate { get; set; }
        public string Iin { get; set; }
    }
}