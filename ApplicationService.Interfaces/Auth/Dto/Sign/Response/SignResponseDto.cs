namespace ApplicationService.Interfaces.Auth.Dto.Sign.Response
{
    public class SignResponseDto<T> : SignResultDto
    {
        /// <summary>
        /// Рез-т
        /// </summary>
        public T Result { get; set; }

        /// <summary>
        /// Сообщение
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// Статус
        /// </summary>
        public int Status { get; set; }
    }
}