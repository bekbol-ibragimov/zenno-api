namespace ApplicationService.Interfaces.Auth
{
    public interface ISettingHashingService
    {
        string Crypto(string text);
        string Crypto(string text, string salt);
        string Crypto<T>(T obj, string salt);
        T Decrypto<T>(string text, string salt);
        string Decrypto(string text, string salt);
        string Decrypto(string text);
        string GetHMACSHA1Hash(string text, string key);
    }
}