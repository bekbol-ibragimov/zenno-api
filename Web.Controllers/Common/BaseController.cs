using System.Net;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Web.Controllers.Attributes;
using Web.UseCases.Wrappers;

namespace Web.Controllers.Common
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    [ValidateModel]
    public abstract class BaseController : ControllerBase
    {
        protected readonly string UserId;
        
        protected BaseController()
        {
        }

        protected ApiResponse ResponseOk()
        {
            return ApiResponse.Ok();
        }

        protected ApiResponse<T> ResponseOk<T>(T data)
        {
            return ApiResponse<T>.Ok(data);
        }
        
        protected ApiResponse<T> ResponseCreated<T>(T data)
        {
            Response.StatusCode = (int) HttpStatusCode.Created;
            return ApiResponse<T>.Ok(data);
        }
        
        protected ApiResponse ResponseNoContent()
        {
            Response.StatusCode = (int) HttpStatusCode.NoContent;
            return ApiResponse.Ok();
        }
        
        protected IActionResult ResponseError(string errorMessage, int code = 422)
        {
            switch (code)
            {
                case 422:
                    Response.StatusCode = 422;
                    return UnprocessableEntity(ApiResponse.Error(errorMessage));
                case 400:
                    Response.StatusCode = 400;
                    return BadRequest(ApiResponse.Error(errorMessage));
            }

            Response.StatusCode = 422;
            return UnprocessableEntity(ApiResponse.Error(errorMessage));
        }
        
        protected IActionResult ResponseNotFound(string message)
        {
            Response.StatusCode = 404;
            return NotFound(ApiResponse.Error(message));
        }

        protected IActionResult ResponseError(int code = 422)
        {
            switch (code)
            {
                case 422:
                    Response.StatusCode = 422;
                    return UnprocessableEntity(ApiResponse.Error(ModelState));
                case 400:
                    Response.StatusCode = 400;
                    return BadRequest(ApiResponse.Error(ModelState));
            }

            Response.StatusCode = 422;
            return UnprocessableEntity(ApiResponse.Error(ModelState));
        }
    }
}