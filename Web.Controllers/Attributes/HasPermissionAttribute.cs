﻿using System.Linq;
using Entities.Enums;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Web.UseCases.Enums;

namespace Web.Controllers.Attributes
{
    public class HasPermissionAttribute : TypeFilterAttribute
    {
        public HasPermissionAttribute(params PermissionEnum[] permissions) : base(typeof(PermissionFilter))
        {
            Arguments = new[] { new PermissionRequirement(permissions) };
        }
    }
    
    public class PermissionFilter : IAuthorizationFilter
    {
        private readonly PermissionRequirement _permissionRequirement;
        
        public PermissionFilter(PermissionRequirement permissionRequirement)
        {
            _permissionRequirement = permissionRequirement;
        }


        public void  OnAuthorization(AuthorizationFilterContext context)
        {
            var claims = context.HttpContext.User.Claims.Where(c => c.Type == ClaimType.Permission)
                .Select(x => x.Value).ToArray();
            if(claims.Contains(PermissionEnum.Sa.ToString())) 
                return;
            
            var permissions = _permissionRequirement.Permissions.Select(x => x.ToString()).ToArray();
            
            var hasClaims = permissions.Intersect(claims).ToArray();
            if (hasClaims.Length == 0)
            {
                context.Result = new ForbidResult();
            }
        }
    }
    public class PermissionRequirement : IAuthorizationRequirement
    {
        public PermissionEnum[] Permissions { get; set; }
        public PermissionRequirement(PermissionEnum[] permissions)
        {
            Permissions = permissions;
        }
    }

}
