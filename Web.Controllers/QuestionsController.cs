﻿using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Utils.Pagination;
using Web.Controllers.Common;
using Web.UseCases.Questions.Commands;
using Web.UseCases.Questions.Dto;
using Web.UseCases.Questions.Queries;
using Web.UseCases.Wrappers;

namespace Web.Controllers
{
    [AllowAnonymous]
    public class QuestionsController: BaseController
    {
        private readonly ISender _sender;

        public QuestionsController(ISender sender)
        {
            _sender = sender;
        }
        
        [HttpGet]
        public async Task<ApiResponse<PaginationResponse<QuestionOdto>>> GetList([FromQuery] QuestionFilter filter)
        {
            
            var result = await _sender.Send(new QuestionGetListQuery {Filter = filter});
            return ResponseOk(result);
        }
        
       
        [RequestSizeLimit(100_000_000)]
        [HttpPost("[action]")]
        public async Task<ApiResponse<QuestionOdto>>  Add([FromForm] QuestionAddIdto dto)
        {
            var result = await _sender.Send(new QuestionAddCommand {Dto = dto});
            return ResponseCreated(result);
        }
    }
}