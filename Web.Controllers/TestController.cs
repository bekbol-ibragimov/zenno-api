﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Web.Controllers.Common;


namespace Web.Controllers
{
    [AllowAnonymous]
    public class TestController: BaseController
    {
        private readonly IMapper _mapper;


        public TestController(IMapper mapper)
        {
            _mapper = mapper;
        }
        

        [HttpGet("[action]")]
        public string Test()
        {
            return null;
        }
    }
}