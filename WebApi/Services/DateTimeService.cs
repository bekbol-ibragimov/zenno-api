﻿using System;
using WebApp.Interfaces.Services;

namespace WebApi.Services
{
    public class DateTimeService : IDateTimeService
    {
        public DateTime NowUtc => DateTime.UtcNow;
    }
}