﻿ using Microsoft.AspNetCore.Builder;

 namespace WebApi.Swagger
{
    public static class ApplicationBuilderExtensions
    {
        public static void UseSwaggerDocumentation(this IApplicationBuilder app)
        {
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint($"/swagger/{nameof(ApiClient.Web)}/swagger.json", "Hr API Web");
            });
        }
    }
}