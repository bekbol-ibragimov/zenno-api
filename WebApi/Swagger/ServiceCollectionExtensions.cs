﻿using System;
using System.Collections.Generic;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Any;
using Microsoft.OpenApi.Interfaces;
using Microsoft.OpenApi.Models;

namespace WebApi.Swagger
{
    public static class ServiceCollectionExtensions
    {
        public static void ConfigureSwagger(this IServiceCollection services)
        {
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc(nameof(ApiClient.Web), new OpenApiInfo {Title = "HR Api web", Version = "v1"});
                c.SwaggerDoc(nameof(ApiClient.Mobile), new OpenApiInfo {Title = "HR Api mobile", Version = "v1"});

                c.AddSecurityDefinition("oauth2", new OpenApiSecurityScheme
                {
                    Type = SecuritySchemeType.OAuth2,
                    Flows = new OpenApiOAuthFlows
                    {
                        Password = new OpenApiOAuthFlow
                        {
                            TokenUrl = new Uri("/api/swagger/token", UriKind.Relative),
                            Extensions = new Dictionary<string, IOpenApiExtension>
                            {
                                {"returnSecureToken", new OpenApiBoolean(true)},
                            }
                        }
                    }
                });
                c.OperationFilter<AuthorizeCheckOperationFilter>();
            });
        }
        
    }
}