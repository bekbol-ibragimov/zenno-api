using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using ApplicationService.Interfaces.Auth;
using ApplicationService.Interfaces.Auth.Dto.Sign.Request;
using ApplicationService.Interfaces.Auth.Dto.Sign.Response;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace ApplicationService.Implementation.Auth
{
    /// <summary>
    /// Логика подписания документов через ЭЦП НУЦ РК
    /// </summary>
    public class SignFileControlService : ISignFileControlService
    {
        private readonly IConfiguration _configuration;


        public SignFileControlService(IConfiguration configuration)
        {
            _configuration = configuration;
            
        }

        public async Task<SignResponseDto<SignResultDto>> ValidateSign(string singBase64)
        {
            var signRequest = new SignCheckRequestDto
            {
                Version = "2.0",
                Method = "cms.verify",
                Params = new SignCheckParamDto
                {
                    VerifyOcsp = true,
                    VerifyCrl = true,
                    Cms = singBase64
                }
            };

            var json = JsonConvert.SerializeObject(signRequest, Formatting.Indented);
            var responseString = await SendRequestPost(json);
            var data = JsonConvert.DeserializeObject<SignResponseDto<SignResultDto>>(responseString);
            return data;

        }
        public async Task<SignExtractResponseDto> ExtractRaw(string singBase64)
        {
            var signRequest = new SignCheckRequestDto
            {
                Version = "2.0",
                Method = "cms.extract",
                Params = new SignCheckParamDto
                {
                    Cms = singBase64
                }
            };

            var json = JsonConvert.SerializeObject(signRequest, Formatting.Indented);
            var responseString = await SendRequestPost(json);
            var data = JsonConvert.DeserializeObject<SignExtractResponseDto>(responseString);
            return data;

        }

        public string GetSingErrorMessage(int status)
        {
            switch (status)
            {
                case 3: return "Не валидная подпись ключом ЭЦП";
                case 5: return "Срок действия сертификата истек";
                default:
                    return "Ошибка подписания #" + status;
            }
        }
        
        /// <summary>
        /// Отправка запроса на докер для подписания через ЭЦП
        /// </summary>
        /// <param name="json"></param>
        /// <returns></returns>
        /// <exception cref="HttpRequestException"></exception>
        private async Task<string> SendRequestPost(string json)
        {
            var httpContent = new StringContent(json, Encoding.UTF8, "application/json");
            httpContent.Headers.ContentType.CharSet = "";
            using var client = new HttpClient();
            var response = await client.PostAsync(_configuration.GetValue<string>("SignServer:Url"), httpContent);
            if (!response.IsSuccessStatusCode)
            {
                throw new HttpRequestException("SignValidate service provider is not available");
            }

            return response.Content.ReadAsStringAsync().Result;
        }
        
     
    }
}