using System;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using ApplicationService.Interfaces.Auth;
using Newtonsoft.Json;

namespace ApplicationService.Implementation.Auth
{
    public class SettingHashingService : ISettingHashingService
    {
        private string _salt = "92#4w)}CMyMK&uKx]<B_Hk@VSvg$H!7DQ+SzNc*qg<yT'g_LySQscP9#a`&aeLjX>D";

        public string Crypto(string text)
        {
            return crypto(text, _salt);
        }

        public string Crypto(string text, string salt)
        {
            return crypto(text, salt);
        }

        public string Crypto<T>(T obj, string salt)
        {
            var str = JsonConvert.SerializeObject(obj);
            return crypto(str, salt);
        }

        private string crypto(string text, string salt)
        {
            try
            {
                var provider = new MD5CryptoServiceProvider();
                byte[] toEncryptArray = Encoding.UTF8.GetBytes(text);
                byte[] keyArray = provider.ComputeHash(Encoding.UTF8.GetBytes(salt));
                provider.Clear();

                TripleDESCryptoServiceProvider triple = new TripleDESCryptoServiceProvider();

                triple.Key = keyArray;
                triple.Mode = CipherMode.ECB;
                triple.Padding = PaddingMode.PKCS7;
                ICryptoTransform cryptoTransform = triple.CreateEncryptor();

                byte[] resultArray = cryptoTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);
                return Convert.ToBase64String(resultArray, 0, resultArray.Length);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public T Decrypto<T>(string text, string salt)
        {
            var json = decrypto(text, salt);
            return JsonConvert.DeserializeObject<T>(json);
        }

        public string Decrypto(string text, string salt)
        {
            return decrypto(text, salt);
        }


        public string Decrypto(string text)
        {
            return decrypto(text, _salt);
        }


        public string GetHMACSHA1Hash(string text, string key)
        {
            var plainText = ShaHash(text, key);
            var plainTextBytes = Encoding.UTF8.GetBytes(plainText);
            return Convert.ToBase64String(plainTextBytes);
        }

        private string ShaHash(string value, string key)
        {
            using (var hmac = new HMACSHA1(Encoding.ASCII.GetBytes(key)))
            {
                var s = hmac.ComputeHash(Encoding.ASCII.GetBytes(value));
                return string.Concat(s.Select(b => b.ToString("x2")));
            }
        }

        private string decrypto(string text, string salt)
        {
            try
            {
                var hashmd5 = new MD5CryptoServiceProvider();
                byte[] toEncryptArray = Convert.FromBase64String(text);
                byte[] keyArray = hashmd5.ComputeHash(Encoding.UTF8.GetBytes(salt));
                hashmd5.Clear();

                TripleDESCryptoServiceProvider triple = new TripleDESCryptoServiceProvider();

                triple.Key = keyArray;
                triple.Mode = CipherMode.ECB;
                triple.Padding = PaddingMode.PKCS7;
                ICryptoTransform cTransform = triple.CreateDecryptor();
                byte[] resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);
                triple.Clear();
                return Encoding.UTF8.GetString(resultArray);
            }
            catch (Exception e)
            {
                Console.Error.WriteLine(e);
                return text;
            }
        }
    }
}