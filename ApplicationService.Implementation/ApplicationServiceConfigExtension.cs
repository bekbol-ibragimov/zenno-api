﻿using ApplicationService.Implementation.Auth;
using ApplicationService.Interfaces.Auth;
using Microsoft.Extensions.DependencyInjection;

namespace ApplicationService.Implementation
{
    public static class ApplicationServiceConfigExtension
    {
        public static void RegisterApplicationService(this IServiceCollection services)
        {
            services.AddTransient<ISignFileControlService, SignFileControlService>();
            services.AddSingleton<ISettingHashingService, SettingHashingService>();
        }
    }
}