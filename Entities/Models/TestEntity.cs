﻿using Entities.Models.Global;

namespace Entities.Models
{
    public class TestEntity: BaseEntity
    {
        public string Name { get; set; }
    }
}