﻿﻿using Entities.Models.Global;

 namespace Entities.Models.Files
{
    public abstract class BaseFileEntity: BaseEntity
    {
        /// <summary>
        /// Путь до файла
        /// </summary>
        public string FullPath { get; set; }
        
        public string FileName { get; set; }
        
        public string OriginalFileName { get; set; }
        
        /// <summary>
        /// Расширение файла
        /// </summary>       
        public string Extension { get; set; }
        
    }
}