﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Entities.Models.Global;

namespace Entities.Models.Auth
{
    public class UserRole: BaseEntity
    {
        public Guid UserId { get; set; }
        public Guid RoleId { get; set; }
        
        
        
        
        //--------------------------------------------------------------------------------------------------------------
        
        [ForeignKey(nameof(UserId))]
        public User User { get; set; }
        
        [ForeignKey(nameof(RoleId))]
        public Role Role { get; set; }
    }
}