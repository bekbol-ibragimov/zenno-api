﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Entities.Models.Global;

namespace Entities.Models.Auth
{
    public class UserPermission: BaseEntity
    {
        public Guid UserId { get; set; }
        public Guid PermissionId { get; set; }
        
        
        
        
        //--------------------------------------------------------------------------------------------------------------
        
        [ForeignKey(nameof(UserId))]
        public User User { get; set; }
        
        [ForeignKey(nameof(PermissionId))]
        public Permission Permission { get; set; }
    }
}