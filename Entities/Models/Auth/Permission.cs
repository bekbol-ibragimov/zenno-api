﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Entities.Enums;
using Entities.Models.Global;

namespace Entities.Models.Auth
{
    public class Permission: BaseEntity
    {
        public Permission()
        {
            UserRoles = new HashSet<UserPermission>();
        }

        [Required]
        public PermissionEnum Code { get; set; }
        [Required]
        public string NameRu { get; set; }
        public string NameKz { get; set; }
        public string NameEn { get; set; }
        
        //--------------------------------------------------------------------------------------------------------------
        public IEnumerable<UserPermission> UserRoles { get; set; }
    }
}