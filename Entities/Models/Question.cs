﻿using System;
using Entities.Enums;
using Entities.Models.Global;

namespace Entities.Models
{
    public class Question: BaseEntity
    {
        public string Name { get; set; }
        public DateTime CreateDate { get; set; }
        public bool ContainsCyrillic { get; set; }
        public bool ContainsLatin { get; set; }
        public bool ContainsNumbers { get; set; }
        public bool ContainsSpecialCharacters { get; set; }
        public bool CaseSensitivity { get; set; }
        public LocationOfAnswersEnum LocationOfAnswers { get; set; }
        public string FilePath { get; set; }
        public string OriginalFileName { get; set; }
        
    }
}