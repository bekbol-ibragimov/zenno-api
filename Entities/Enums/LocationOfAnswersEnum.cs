﻿namespace Entities.Enums
{
    /// <summary>
    /// Расположение ответов на картинки
    /// </summary>
    public enum LocationOfAnswersEnum
    {
        /// <summary>
        /// отсутствует
        /// </summary>
        Absent = 0,
        /// <summary>
        /// в именах файлов
        /// </summary>
        InFilenames = 1,
        /// <summary>
        /// в отдельном файле
        /// </summary>
        InSeparateFile = 2
    }
}