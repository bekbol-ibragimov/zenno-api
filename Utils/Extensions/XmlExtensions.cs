﻿using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace Utils.Extensions
{
    public static class XmlExtensions
    {
        public static string ToXml<T>(this T obj, bool beautify = false)
            where T : class
        {
            //var emptyNamespaces = new XmlSerializerNamespaces(new[] { XmlQualifiedName.Empty });
            var serializer = new XmlSerializer(obj.GetType());
            var settings = new XmlWriterSettings {Indent = beautify, OmitXmlDeclaration = true};
            using var stream = new StringWriter();
            using var writer = XmlWriter.Create(stream, settings);
            serializer.Serialize(writer, obj);
            return stream.ToString();
        }

        public static T FromXml<T>(this string xml) where T : class
        {
            var strReader = new StringReader(xml);
            var serializer = new XmlSerializer(typeof(T));
            var xmlReader = new XmlTextReader(strReader);
            T obj = (T)serializer.Deserialize(xmlReader);
            return obj;
        }
    }
}