﻿using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Linq;
using System.Xml.Serialization;
using Utils.Extensions;

namespace Utils.Soap
{
    [XmlRoot(ElementName="Envelope", Namespace="http://schemas.xmlsoap.org/soap/envelope/")]
    public class Envelope<T>
        where T: class {
        [XmlNamespaceDeclarations]
        public XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
        
        public Envelope()
        {
            xmlns.Add("soap", "http://schemas.xmlsoap.org/soap/envelope/");        
        } 
        
        public string Header { get; set; } = string.Empty;
        public Body<T> Body { get; set; } = new Body<T>();
    }
    

    public class Body<T> {
        [XmlIgnore]
        public T Value { get; set; }
        
        [XmlAnyElement]
        [Browsable(false), EditorBrowsable(EditorBrowsableState.Never), DebuggerBrowsable(DebuggerBrowsableState.Never)]
        public XElement XmlEntity
        {
            get
            {
                return (Value == null ? null : XObjectExtensions.SerializeToXElement(Value, null, true));
            }
            set
            {
                Value = (value == null ? default : XObjectExtensions.Deserialize<T>(value));
            }
        }
    }
}