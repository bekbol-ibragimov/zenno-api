﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Utils.Settings
{
    public static class AppSettingsExtensions
    {
        public static void ConfigureSettings(this IServiceCollection services, IConfiguration configuration)
        {
            var teleport = configuration.GetSection("Teleport");
            var settings = new AppSettings()
            {
                Teleport = new Teleport()
                {
                    Url = teleport.GetValue<string>("Url"),
                }
            };
            services.AddSingleton(settings);
        }
    }



}