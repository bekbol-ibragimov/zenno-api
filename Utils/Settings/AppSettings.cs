﻿namespace Utils.Settings
{
    public class AppSettings
    {
        public Teleport Teleport { get; set; }
    }


    public class Teleport
    {
        public string Url { get; set; }
    }
}