﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace Utils.Validators
{
    public class ValidEnumValueAttribute : ValidationAttribute
    {
        private readonly int[] allowedValues;
        public ValidEnumValueAttribute() { }
        public ValidEnumValueAttribute(int[] allowedValues)
        {
            this.allowedValues = allowedValues;
        }
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if(value is IEnumerable values)
            {
                List<string> badValues = new List<string>();
                Type enumType = null;
                foreach(object enumValue in values)
                {
                    if (!IsItemValid(enumValue))
                    {
                        badValues.Add(enumValue.ToString());
                        if (enumType == null)
                        {
                            enumType = enumValue.GetType();
                        }
                    }
                }
                if (badValues.Any())
                {
                    var badValue = badValues.Aggregate((x, y) => x + "," + y);
                    var msg = allowedValues != null
                        ? $"These values are not valid: {badValue}. Only following values from type {enumType.Name} are supported: {allowedValues.Select(x => Enum.GetName(enumType, x)).Aggregate((x, y) => x + ", " + y)}"
                        : $"Following values are not valid for type {enumType.Name}: {badValue}";
                    
                    return new ValidationResult(msg); 
                }  
            }
            else
            {
                if (!IsItemValid(value))
                {
                    var msg = allowedValues != null 
                        ? $"{value} is not a valid. Only following values from type {value.GetType().Name} are supported: {allowedValues.Select(x => Enum.GetName(value.GetType(), x)).Aggregate((x, y) => x + ", " + y)}"
                        :$"{value} is not a valid value for type {value.GetType().Name}";
                    return new ValidationResult(msg);
                }
            } 

            return ValidationResult.Success;
        }   
      

        private bool IsItemValid(object value)
        {  
            if(value is Enum)
            {
                Type enumType = value.GetType();
                bool valid = Enum.IsDefined(enumType, value);
                if (!valid)
                {
                    return false;
                }
                if (allowedValues != null)
                {
                    return allowedValues.Contains((int)value);
                }
            }                 

            return true;
        }
    }
}