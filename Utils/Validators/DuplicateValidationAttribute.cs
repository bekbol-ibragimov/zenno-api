﻿using System;
using System.Collections;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace Utils.Validators
{
    public class DuplicateValidationAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if(value is IEnumerable enumerable)
            {
                var values = enumerable.Cast<object>();
                var badValues = values.GroupBy(x => x).Where(x => x.Count() > 1).Select(x => x.Key).ToList();

                if (badValues.Any())
                {
                    var badValue = string.Join(", ", badValues);
                    var msg = $"Following values are duplicate: {badValue}";
                    return new ValidationResult(msg); 
                }  
            }
            else
            {
                throw new Exception("DuplicateValidationAttribute value is not IEnumerable");
            }
            
            return ValidationResult.Success;
        }

    }
}