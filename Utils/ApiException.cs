﻿using System;
using System.Collections.Generic;
using System.Net;

namespace Utils
{
    public class ApiException : Exception
    {
        public int StatusCode { get; set; }
        public HttpErrorResult Error { get; set; } = new HttpErrorResult();
        
        public ApiException(HttpStatusCode statusCode,  string message): base(message)
        {
            StatusCode = (int) statusCode;
            Error.Status = (int) statusCode;
            Error.Errors.Add(new Error(){Property = "error", Message = message});
        }

        public static ApiException BadRequest()
        {
            return new ApiException(HttpStatusCode.BadRequest, "Bad Request");
        }
        public static ApiException BadRequest(string message)
        {
            return new ApiException(HttpStatusCode.BadRequest, message);
        }
        public static ApiException Unauthorized()
        {
            return new ApiException(HttpStatusCode.Unauthorized, "Unauthorized");
        }
        public static ApiException NotFound()
        {
            return new ApiException(HttpStatusCode.NotFound, "Not Found");
        }
        public static ApiException NotFound(string param)
        {
            return new ApiException(HttpStatusCode.NotFound, $"Not Found {param}");
        }
        
        public static ApiException NotFound(string param, string values)
        {
            return new ApiException(HttpStatusCode.NotFound, $"Not Found {param}: {values}");
        }
        
        public static ApiException Conflict (string message)
        {
            return new ApiException(HttpStatusCode.Conflict, message+ " is duplicate");
        }
        
    }
    
    public class Error
    {
        public string Property { get; set; }
        public string Message { get; set; }
    }
    
    public class HttpErrorResult
    {
        public int Status { get; set; }
        public List<Error> Errors { get; set; } = new List<Error>();
    }
    
}