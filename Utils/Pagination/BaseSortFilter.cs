﻿using System;
using Utils.Enums;
using Utils.Validators;

namespace Utils.Pagination
{
    public class BaseSortFilter<TSortField> where TSortField:  struct, Enum
    {
        /// <summary>
        /// Поле сортировки
        /// </summary>
        [ValidEnumValue]
        public TSortField? SortField { get; set; }
        /// <summary>
        /// Направление сортировки
        /// </summary>
        [ValidEnumValue]
        public SortDirection SortDirection { get; set; } = SortDirection.Ascending;
    }
}