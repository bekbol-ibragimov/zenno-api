﻿using System;
using System.Collections.Generic;
using Utils.Enums;

namespace Utils.Pagination
{
    public class PaginationResponse<TResponse>
    {
        public int Total { get; set; }
        public List<TResponse> Items { get; set; }
        
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public Enum SortField { get; set; }
        public SortDirection SortDirection { get; set; }
    }
    
}