﻿using AutoMapper;
using Entities.Models;
using Web.UseCases.Questions.Dto;

namespace Web.UseCases
{
    public class WebUseCasesMapperProfile : Profile
    {
        public WebUseCasesMapperProfile()
        {
            
            CreateMap<QuestionAddIdto, Question>();
            CreateMap<Question, QuestionOdto>();


            
        }


        private void UserMapped()
        {

        }
    }
}