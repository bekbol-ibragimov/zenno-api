﻿using System;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using ApplicationService.Interfaces.Enums;
using AutoMapper;
using DataAccess.Interfaces;
using Entities.Enums;
using Entities.Models;
using MediatR;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Utils;
using Web.UseCases.Questions.Dto;

namespace Web.UseCases.Questions.Commands
{
    public class QuestionAddCommand: IRequest<QuestionOdto>
    {
        public QuestionAddIdto Dto { get; set; }
        
        public class Handler : IRequestHandler<QuestionAddCommand, QuestionOdto>
        {
            private readonly string _rootPath;
            private readonly IMsDbContext _msDb;
            private readonly IMapper _mapper;
            
            public Handler( IWebHostEnvironment environment, IMsDbContext msDb, IMapper mapper)
            {
                _msDb = msDb;
                _mapper = mapper;
                _rootPath = environment.WebRootPath;
            }

            public async Task<QuestionOdto> Handle(QuestionAddCommand request, CancellationToken cancellationToken)
            {
                var dto = request.Dto;

                if (dto.File == null || dto.File.Length == 0)
                    throw ApiException.BadRequest("Архив файл не найден");

                var minCount = 2000;
                var addCount = 3000;
                if (dto.ContainsCyrillic)
                    minCount += addCount;
                if (dto.ContainsLatin)
                    minCount += addCount;
                if (dto.ContainsNumbers)
                    minCount += addCount;
                if (dto.ContainsSpecialCharacters)
                    minCount += addCount;
                if (dto.CaseSensitivity)
                    minCount += addCount;

                var maxCount = minCount + 1000;

                using (var ms = new MemoryStream())
                {
                    await dto.File.CopyToAsync(ms);

                    using (ZipArchive archive = new ZipArchive(ms))
                    {
                        var jpgCount = archive.Entries.Count(x => x.Name.Contains(".jpg"));
                        if (jpgCount < minCount || jpgCount > maxCount)
                            throw ApiException.BadRequest(
                                $"Количество картинок в архиве не в диапазоне {minCount}-{maxCount}");
                        if (dto.LocationOfAnswers == LocationOfAnswersEnum.InSeparateFile)
                        {
                            var answersFile = archive.Entries.FirstOrDefault(x => x.Name == "answers.txt");
                            if (answersFile == null)
                                throw ApiException.BadRequest(
                                    $"Присутствует файл с ответами, когда указано, что ответы в файле");

                            using (StreamReader reader = new StreamReader(answersFile.Open()))
                            {
                                string txt = reader.ReadToEnd();
                                var answers = txt.Split(new char[] {'\n'}).Where(x => x.Length > 0); // big array
                                var answersCount = answers.Count();
                                if (answersCount != jpgCount)
                                    throw ApiException.BadRequest(
                                        $"Количество ответов({answersCount}) не совпадает с количеством картинок({jpgCount})");
                            }
                        }
                    }
                }
                var fullPath = await SaveFile(dto.File);
                    
                var question = _mapper.Map<Question>(request.Dto);
                question.FilePath = fullPath;
                question.OriginalFileName = dto.File.Name;
                _msDb.Questions.Add(question);
                await _msDb.SaveChangesAsync(cancellationToken);

                return _mapper.Map<QuestionOdto>(question);
            }
            
            
            public async Task<string> SaveFile(IFormFile file)
            {
                var guid = Guid.NewGuid();
                var extension = Path.GetExtension(file.FileName);
                var newFilename = $"{guid.ToString()}{extension}";
                var fullPath = $"/{FilePath.Files.ToString()}/{FilePath.Questions.ToString()}/{newFilename}";
                var newFilePath = $"{_rootPath}{fullPath}";
                ValidatePath(newFilePath);
                await using (var stream = new FileStream(newFilePath, FileMode.CreateNew))
                {
                    await file.CopyToAsync(stream);
                }

                return fullPath;
            }
            
            private void ValidatePath(string filePath)
            {
                var dirPath = Path.GetDirectoryName(filePath);
                if (!Directory.Exists(dirPath))
                {
                    Directory.CreateDirectory(dirPath);
                }
            }
        }
    }
}