﻿using System;
using System.ComponentModel.DataAnnotations;
using Entities.Enums;
using FluentValidation;
using Microsoft.AspNetCore.Http;

namespace Web.UseCases.Questions.Dto
{
    public class QuestionAddIdto
    {
        [RegularExpression("^[a-zA-Z]{4,8}$",ErrorMessage = "Имя содержит только латинские буквы; Максимальная длина имени - от 4 до 8 символов;")]
        public string Name { get; set; }
        public DateTime CreateDate { get; set; }
        public bool ContainsCyrillic { get; set; }
        public bool ContainsLatin { get; set; }
        public bool ContainsNumbers { get; set; }
        public bool ContainsSpecialCharacters { get; set; }
        public bool CaseSensitivity { get; set; }
        public LocationOfAnswersEnum LocationOfAnswers { get; set; }
        public IFormFile File { get; set; }
    }
    
    public class QuestionAddIdtoValidator : AbstractValidator<QuestionAddIdto>
    {
        public QuestionAddIdtoValidator()
        {
            RuleFor(x => x.Name).NotNull();
            When(x => x.Name != null, () => {
                RuleFor(x => x.Name)
                    .Must(x => !x.Contains("captcha"))
                    .WithMessage("Имя не может содержать слово “captcha”;");
            });
            RuleFor(x => x)
                .Must(x => x.ContainsCyrillic || x.ContainsLatin || x.ContainsNumbers)
                .WithMessage("Выбрите как минимум одно из: “Содержит кириллицу”, “Содержит латиницу”, “Содержит цифры”;").WithName("Question");
            
        }
    }
}