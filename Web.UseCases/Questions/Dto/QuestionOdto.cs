﻿using System;
using System.ComponentModel.DataAnnotations;
using Entities.Enums;
using FluentValidation;
using Microsoft.AspNetCore.Http;

namespace Web.UseCases.Questions.Dto
{
    public class QuestionOdto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public DateTime CreateDate { get; set; }
        public bool ContainsCyrillic { get; set; }
        public bool ContainsLatin { get; set; }
        public bool ContainsNumbers { get; set; }
        public bool ContainsSpecialCharacters { get; set; }
        public bool CaseSensitivity { get; set; }
        public LocationOfAnswersEnum LocationOfAnswers { get; set; }
        public string FilePath { get; set; }
    }
    

}