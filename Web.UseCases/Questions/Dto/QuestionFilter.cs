﻿using Utils.Pagination;

namespace Web.UseCases.Questions.Dto
{
    public class QuestionFilter: PaginationRequest<QuestionOrderBy>
    {
        public string SearchString { get; set; }
    }
    
    public enum QuestionOrderBy
    {
        Id,
        Name,
    }
}