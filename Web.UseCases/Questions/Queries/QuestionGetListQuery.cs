﻿using System;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using ApplicationService.Interfaces.Enums;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using DataAccess.Interfaces;
using Entities.Enums;
using Entities.Models;
using MediatR;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Utils;
using Utils.Pagination;
using Web.UseCases.Questions.Dto;

namespace Web.UseCases.Questions.Queries
{
    public class QuestionGetListQuery: IRequest<PaginationResponse<QuestionOdto>>
    {
        public QuestionFilter Filter { get; set; }
        
        public class Handler : IRequestHandler<QuestionGetListQuery, PaginationResponse<QuestionOdto>>
        {
            private readonly IMsDbContext _msDb;
            private readonly IMapper _mapper;
            
            public Handler( IMsDbContext msDb, IMapper mapper)
            {
                _msDb = msDb;
                _mapper = mapper;
            }

            public async Task<PaginationResponse<QuestionOdto>> Handle(QuestionGetListQuery request, CancellationToken cancellationToken)
            {
                var result = await _msDb.Questions.Where(x => x.IsDeleted == false)
                    .Where(x => string.IsNullOrEmpty(request.Filter.SearchString) ||
                                x.Name.ToLower().Contains(request.Filter.SearchString.ToLower()))
                    .AsNoTracking()
                    .OrderBy(request.Filter)
                    .ProjectTo<QuestionOdto>(_mapper.ConfigurationProvider)
                    .ToPaginationListAsync(request.Filter);
                return result;
            }
            
        }
    }
}