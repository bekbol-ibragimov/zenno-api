﻿namespace Web.UseCases.Enums
{
    public class ClaimType
    {
        /// <summary>ИД Пользавателя</summary>
        public const string UserId = "UserId";
        
        /// <summary>Логин Пользавателя</summary>
        public const string Username = "Username";
        
        /// <summary>Email Пользавателя</summary>
        public const string UserEmail = "Email";
        
        /// <summary>Доступ к модулям</summary>
        public const string Permission = "Permission";
        

        public const string DeviceId = "Email";
        
        public const string PhoneNumberId = "Email";
    }
}