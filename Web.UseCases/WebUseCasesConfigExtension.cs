﻿using System.Reflection;
using FluentValidation;
using MediatR;
using Microsoft.Extensions.DependencyInjection;
using Web.UseCases.Questions.Dto;

namespace Web.UseCases
{
    public static class WebUseCasesConfigExtension
    {
        public static void RegisterWebUseCases(this IServiceCollection services)
        {
            services.AddAutoMapper(Assembly.GetExecutingAssembly());
            services.AddMediatR(Assembly.GetExecutingAssembly());
            
            
            
            
            services.AddSingleton<IValidator<QuestionAddIdto>, QuestionAddIdtoValidator>();
        }
    }
}